﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace AG.Spherium
{
    public class Globals
    {
        public static float WaitTime
        {
            get { return 0.85f / currentLevelMultiplier; }
        }

        public static float rotationRate
        {
            get { return 40 * currentLevelMultiplier; }
        }

        public static float movementMultiplier
        {
            get { return 1.5f; }
        }

        private static readonly float[] levelSpeedMultipliers = {1.0f, 1.1f, 1.2f, 1.4f, 1.5f, 1.7f, 2f, 2.5f};

        private static readonly int[] levelUpScores = {5, 10, 15, 30, 45, 60, 75, 90, 105};

        public static ushort currentLevel;

        private static ushort currentScore;

        public static ushort CurrentScore
        {
            get { return currentScore; }
            set
            {
                currentScore = value;
                if (currentScore > nextLevelScore)
                {
                    currentLevel = Convert.ToUInt16(Mathf.Clamp(currentLevel+1, 0, maxLevel));
                }
            }
        }

        public static ushort maxLevel = 8;

        public static float currentLevelMultiplier
        {
            get { return levelSpeedMultipliers[Mathf.Clamp(currentLevel-1, 0, maxLevel)]; }
        }

        public static int nextLevelScore
        {
            get { return levelUpScores[Mathf.Clamp(currentLevel, 0, maxLevel)]; }
        }

        //Language
        public static int currentLanguage;

        public static void Load()
        {
            //CurrentScore = (ushort)PlayerPrefs.GetInt("CurrentScore", 0);
            //currentLevel = (ushort)PlayerPrefs.GetInt("CurrentLevel", 0);
            currentLanguage = PlayerPrefs.GetInt("CurrentLanguage", 0);
            Localization.SetLocalization(currentLanguage);
        }

        public static void Save()
        {
            PlayerPrefs.SetInt("CurrentScore", currentScore);
            PlayerPrefs.SetInt("CurrentLevel", currentLevel);
            PlayerPrefs.SetInt("CurrentLanguage", currentLanguage);
            PlayerPrefs.Save();
        }

        public static void Reset()
        {
            currentLevel = 0;
            currentScore = 0;
        }
    }
}
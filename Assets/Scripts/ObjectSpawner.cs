﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AG.Spherium
{

    public class ObjectSpawner : MonoBehaviour
    {
        private GameManager gameManager;

        public GameObject[] LevelObjects;
        public Material[] ObjectMaterials;

        private int objectsCount;

        private bool isSpawning;

        void Start()
        {
            gameManager = GetComponent<GameManager>();
        }

        public void Update()
        {
            if (objectsCount < 8 && !isSpawning)
            {
                StartCoroutine(SpawnSpheres());
            }
        }

        public IEnumerator SpawnSpheres()
        {
            isSpawning = true;
            while (objectsCount < 8)
            {
                int objectType = Random.Range(0, LevelObjects.Length);
                int objectColor = Random.Range(0, ObjectMaterials.Length);

                GameObject obj = Instantiate<GameObject>(LevelObjects[objectType], GetSpawnPosition(), Quaternion.identity);
                obj.GetComponent<Renderer>().sharedMaterial = ObjectMaterials[objectColor];

                var objScript = obj.GetComponent<ClickObject>();
                objScript.objectColor = (ObjectColor)objectColor;
                objScript.objectType = (ObjectType)objectType;
                objScript.ObjectDestroyed += ObjectDestroyed;

                objectsCount++;
                yield return new WaitForSeconds(Globals.WaitTime);
            }
            isSpawning = false;
        }

        private Vector3 GetSpawnPosition()
        {
            return new Vector3((Random.Range(CameraUtils.cameraCorners[1].x-1,CameraUtils.cameraCorners[0].x+1)), CameraUtils.cameraCorners[0].y + 1, 0);
        }

        public void ObjectDestroyed(bool onClick, ObjectType type, ObjectColor color)
        {
            if (onClick)
            {
                gameManager.ObjectDestroyed(type, color);
            }
            objectsCount--;
        }
    }
}

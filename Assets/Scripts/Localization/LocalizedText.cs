﻿using UnityEngine;
using UnityEngine.UI;


public class LocalizedText : MonoBehaviour
{
    public string key;

    private void Awake()
    {
        Localization.OnLanguageChanged += RefreshText;
        RefreshText();
    }

    void RefreshText()
    {
        GetComponent<Text>().text = Localization.GetString(key);
    }

    // Эта функция вызывается при уничтожении MonoBehaviour
    private void OnDestroy()
    {
        Localization.OnLanguageChanged -= RefreshText;
    }


}

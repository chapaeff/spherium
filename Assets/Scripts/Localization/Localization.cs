﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public static class Localization
{
    public delegate void OnLanguageChangedEvent();

    public static event OnLanguageChangedEvent OnLanguageChanged;

    public static string currentLanguage = "RUS";
    private static int languageInt = 0;

    public static void SetLocalization(int value)
    {
        if(value > LangDictionaries.Length) return;
        currentLanguage = LangDictionaries[value]["shortName"];
        languageInt = value;

        if (OnLanguageChanged != null) OnLanguageChanged();
    }

    public static string[] GetLocalizationNames()
    {
        string[] locales = new string[LangDictionaries.Length];

        for (int i = 0; i < locales.Length; i++)
        {
            locales[i] = LangDictionaries[i]["fullName"];
        }

        return locales;
    }


    private static readonly Dictionary<string, string> RusDictionary = new Dictionary<string, string>()
        {
            {"shortName", "RUS"},
            {"fullName" , "Russian"},
            {"newGameButton", "НОВАЯ ИГРА"},
            {"resumeGameButton", "ПРОДОЛЖИТЬ"},
            {"settingsButton", "НАСТРОЙКИ"},
            {"aboutButton", "АВТОРЫ"},
            {"quitGameButton", "ВЫХОД"},
            {"backButton", "НАЗАД"},
            {"levelButton1", "УРОВЕНЬ 1"},
            {"levelButton2", "УРОВЕНЬ 2"},
            {"levelButton3", "УРОВЕНЬ 3"},
            {"levelButton4", "УРОВЕНЬ 4"},
            {"levelButton5", "УРОВЕНЬ 5"},
            {"levelButton6", "УРОВЕНЬ 6"},
            {"levelButton7", "УРОВЕНЬ 7"},
            {"levelButton8", "УРОВЕНЬ 8"},
            {"scoreText", "СЧЕТ: "},
            {"mistakesText", "ОШИБКИ: "},
            {"selectLevelText", "ВЫБРАТЬ УРОВЕНЬ"},
            {"pauseMenuLabel", "ПАУЗА"},
            {"resumeButton", "ПРОДОЛЖИТЬ"},
            {"backToMainButton", "В ГЛАВНОЕ МЕНЮ"},
        };

    private static readonly Dictionary<string, string> EngDictionary = new Dictionary<string, string>()
        {
            {"shortName", "ENG"},
            {"fullName" , "English"},
            {"newGameButton", "NEW GAME"},
            {"resumeGameButton", "CONTINUE"},
            {"settingsButton", "SETTINGS"},
            {"aboutButton", "ABOUT"},
            {"quitGameButton", "QUIT"},
            {"backButton", "BACK"},
            {"levelButton1", "LEVEL 1"},
            {"levelButton2", "LEVEL 2"},
            {"levelButton3", "LEVEL 3"},
            {"levelButton4", "LEVEL 4"},
            {"levelButton5", "LEVEL 5"},
            {"levelButton6", "LEVEL 6"},
            {"levelButton7", "LEVEL 7"},
            {"levelButton8", "LEVEL 8"},
            {"scoreText", "SCORE: "},
            {"mistakesText", "MISTAKES: "},
            {"selectLevelText", "SELECT LEVEL"},
            {"pauseMenuLabel", "GAME PAUSED"},
            {"resumeButton", "CONTINUE"},
            {"backToMainButton", "BACK TO MAIN MENU"},
        };

    public static Dictionary<string, string>[] LangDictionaries = new Dictionary<string, string>[]
    {
        RusDictionary,
        EngDictionary
    };

    public static string GetString(string key)
    {
            if (LangDictionaries[languageInt].ContainsKey(key))
            {
                return LangDictionaries[languageInt][key];
            }
        return "FAIL!";
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AG.Spherium
{
    public class ClickObject : MonoBehaviour
    {
        public delegate void OnObjectDestroyedEvent(bool byClick, ObjectType type, ObjectColor color);

        public OnObjectDestroyedEvent ObjectDestroyed;

        ///

        public ObjectType objectType;

        public ObjectColor objectColor;


        private new Transform transform;

        private Vector3 endVector;

        ///

        // Use this for initialization
        void Start()
        {
            transform = GetComponent<Transform>();

            endVector = new Vector3(transform.position.x, CameraUtils.cameraCorners[1].y - 1);

        }

        //OMG IT CAN BE CALLED FROM ANDROID!!!!
        private void OnMouseDown()
        {
            DestroyObj(true);
        }

        // Метод Update вызывается на каждом кадре, если класс MonoBehaviour включен
        private void Update()
        {
            //Опускаем обьект к нижней границы камеры и на метр ниже, чтобы обьект не исчезал на экране
            transform.position = new Vector3(transform.position.x,
                Mathf.MoveTowards(transform.position.y, CameraUtils.cameraCorners[1].y - 1,
                    Time.deltaTime * Globals.currentLevelMultiplier * Globals.movementMultiplier),
                transform.position.z);

            transform.Rotate(Vector3.forward * Globals.rotationRate * Globals.currentLevelMultiplier * Time.deltaTime);

            if (transform.position == endVector)
            {
                DestroyObj(false);
            }
        }

        private void DestroyObj(bool byClick)
        {
            if(ObjectDestroyed != null) ObjectDestroyed(byClick, objectType, objectColor);
            Destroy(gameObject);
        }


    }
}

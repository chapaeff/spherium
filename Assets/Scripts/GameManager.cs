﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;

namespace AG.Spherium
{
    public class GameManager : MonoBehaviour
    {

        public static int CurrentLevel { get; private set; }

        public int PlayerMistakes;

        public Text scoreText, mistakesText;
        private string scoreTextString, mistakesTextString;
        public Button pauseButton;

        public GameObject pauseObject;
        public Blur blur;

        public Button ExitToMainButton, ResumeButton;


        private void Start()
        {
            scoreTextString = scoreText.text;
            mistakesTextString = mistakesText.text;

            RefreshUI();

            pauseButton.onClick.AddListener(OpenPause);
            ExitToMainButton.onClick.AddListener(() => {SceneManager.LoadScene(0);});
            ResumeButton.onClick.AddListener(ClosePause);
        }

        private void ClosePause()
        {
            pauseObject.SetActive(false);
            blur.enabled = false;
            pauseButton.gameObject.SetActive(true);
            RefreshUI();
            Time.timeScale = 1;
        }

        private void OpenPause()
        {
            pauseObject.SetActive(true);
            Time.timeScale = 0;
            blur.enabled = true;
            pauseButton.gameObject.SetActive(false);
            scoreText.text = "";
            mistakesText.text = "";
        }

        public void ObjectDestroyed(ObjectType type, ObjectColor color)
        {
            if (type == 0)
            {
                PlayerMistakes++;
            }
            else
            {
                Globals.CurrentScore++;
            }

            RefreshUI();
        }

        void RefreshUI()
        {
            scoreText.text = scoreTextString + Globals.CurrentScore;
            mistakesText.text = mistakesTextString + PlayerMistakes;
        }

        private void OnApplicationQuit()
        {
            Globals.Save();
        }
    }
}

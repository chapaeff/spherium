﻿using System.Collections;
using System.Linq;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace AG.Spherium
{
    public class MainMenu : MonoBehaviour
    {
        public GameObject[] tabs;
        private int currentTab = 0;

        
        public Button NewGameButton, ResumeGameButton, SettingsButton, AboutButton, QuitButton;

        public Dropdown LanguageDropdown;

        public Button BackButton;

        public Transform levelSelectionParent;


        public Image blackScreen;



        [Header("LoadingBar")] public Image loadingBar;

        private void Awake()
        {
            Globals.Load();
            Time.timeScale = 1;
            InitButtons();
        }

        private void InitButtons()
        {
            BackButton.onClick.AddListener(() => StartCoroutine(SwitchMenu(0)));
            BackButton.gameObject.SetActive(false);
            blackScreen.CrossFadeAlpha(0, 1f, false);
            blackScreen.raycastTarget = false;
            NewGameButton.onClick.AddListener(() => StartCoroutine(SwitchMenu(1)));
            SettingsButton.onClick.AddListener(() => StartCoroutine(SwitchMenu(2)));
            
            //Options

            LanguageDropdown.ClearOptions();
            LanguageDropdown.AddOptions(Localization.GetLocalizationNames().ToList());
            LanguageDropdown.onValueChanged.AddListener(Localization.SetLocalization);
            LanguageDropdown.value = Globals.currentLanguage;

            for (int i = 0; i < Globals.maxLevel; i++)
            {
                var i1 = i;

                levelSelectionParent.GetChild(i).GetComponent<Button>().onClick.AddListener(() => StartCoroutine(LoadLevel(i1)));
            }

            for (int i = 1; i < tabs.Length; i++)
            {
                tabs[i].SetActive(false);
            }
        }

        private IEnumerator SwitchMenu(int newTab)
        {
            blackScreen.canvasRenderer.SetAlpha(0);

            blackScreen.raycastTarget = true;
            blackScreen.CrossFadeAlpha(1, .5f, false);
            while (blackScreen.canvasRenderer.GetAlpha() != 1f)
            {
                yield return null;
            }
            tabs[currentTab].SetActive(false);
            if (newTab == 0)
            {
                BackButton.gameObject.SetActive(false);
            }
            else if (currentTab == 0)
            {
                BackButton.gameObject.SetActive(true);
            }
            tabs[newTab].SetActive(true);
            blackScreen.CrossFadeAlpha(0, .5f, false);
            while (blackScreen.canvasRenderer.GetAlpha() != 0f)
            {
                yield return null;
            }
            currentTab = newTab;
            blackScreen.raycastTarget = false;
        }

        #region Button Functions

        public IEnumerator LoadLevel(int level)
        {
            loadingBar.gameObject.SetActive(true);
            blackScreen.raycastTarget = true;
            blackScreen.CrossFadeAlpha(1, 0.5f, false);

            while (blackScreen.canvasRenderer.GetAlpha() != 1) yield return null;

            var loader =  SceneManager.LoadSceneAsync(1);

            loader.allowSceneActivation = true;

            while (!loader.isDone)
            {
                loadingBar.rectTransform.localScale = Vector3.right * loader.progress;
                yield return null;
            }
        }

        #endregion
    }
}

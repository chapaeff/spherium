﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AG.Spherium
{

    [RequireComponent(typeof(Camera))]
    public class CameraUtils : MonoBehaviour
    {
        //Caching
        private new Camera camera;

        public static Vector3[] cameraCorners = new Vector3[2];

        // Use this for initialization
        void Start()
        {
            camera = GetComponent<Camera>();

            GetCameraCorners(10);
        }


        void GetCameraCorners(int zPosition)
        {
            cameraCorners[0] = camera.ScreenToWorldPoint(new Vector3(0, Screen.height, zPosition));
            cameraCorners[1] = camera.ScreenToWorldPoint(new Vector3(Screen.width, 0, zPosition));
        }

    }
}